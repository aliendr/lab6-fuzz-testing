import {calculateBonuses} from "./bonus-system.js"

describe('bonus-system tests', () => {
    let app;
    console.log('bonus-system tests started');


    test('Standard < 10 000', (done) => {
        const multiplier = 0.05;
        const bonus = 1;
        expect(calculateBonuses("Standard", -11)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Standard", 0)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Standard", 9999)).toEqual(bonus * multiplier);
        done();
    });

    test('Premium < 10 000', (done) => {
        const multiplier = 0.1;
        const bonus = 1;
        expect(calculateBonuses("Premium", -11)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Premium", 0)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Premium", 9999)).toEqual(bonus * multiplier);
        done()
    });

    test('Diamond < 10 000', (done) => {
        const multiplier = 0.2;
        const bonus = 1;
        expect(calculateBonuses("Diamond", -11)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Diamond", 0)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Diamond", 9999)).toEqual(bonus * multiplier);
        done()
    });

    test('Invalid < 10 000', (done) => {
        const multiplier = 0;
        const bonus = 1;
        expect(calculateBonuses("Invalid", -11)).toEqual(0);
        done()
    });

    test('Standard 10 000 <= x < 50 000', (done) => {
        const multiplier = 0.05;
        const bonus = 1.5;
        expect(calculateBonuses("Standard", 10000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Standard", 25000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Standard", 49999)).toEqual(bonus * multiplier);
        done()
    });

    test('Premium 10 000 <= x < 50 000', (done) => {
        const multiplier = 0.1;
        const bonus = 1.5;
        expect(calculateBonuses("Premium", 10000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Premium", 25000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Premium", 49999)).toEqual(bonus * multiplier);
        done()
    });

    test('Diamond 10 000 <= x < 50 000', (done) => {
        const multiplier = 0.2;
        const bonus = 1.5;
        expect(calculateBonuses("Diamond", 10000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Diamond", 25000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Diamond", 49999)).toEqual(bonus * multiplier);
        done()
    });


    test('Invalid 10 000 <= x < 50 000', (done) => {
        const multiplier = 0;
        const bonus = 1.5;
        expect(calculateBonuses("Invalid", 10000)).toEqual(0);
        expect(calculateBonuses("Invalid", 25000)).toEqual(0);
        expect(calculateBonuses("Invalid", 49999)).toEqual(0);
        done()
    });

    test('Standard 50 000 <= x < 100 000', (done) => {
        const multiplier = 0.05;
        const bonus = 2;
        expect(calculateBonuses("Standard", 50000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Standard", 75000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Standard", 99999)).toEqual(bonus * multiplier);
        done()
    });

    test('Premium 50 000 <= x < 100 000', (done) => {
        const multiplier = 0.1;
        const bonus = 2;
        expect(calculateBonuses("Pr" +
            "emium", 50000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Premium", 75000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Premium", 99999)).toEqual(bonus * multiplier);
        done()
    });

    test('Diamond 50 000 <= x < 100 000', (done) => {
        const multiplier = 0.2;
        const bonus = 2;
        expect(calculateBonuses("Diamond", 50000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Diamond", 75000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Diamond", 90000)).toEqual(bonus * multiplier);
        done()
    });

    test('Invalid 50 000 <= x < 100 000', (done) => {
        const multiplier = 0;
        const bonus = 2;
        expect(calculateBonuses("Invalid", 50000)).toEqual(0);
        expect(calculateBonuses("Invalid", 75000)).toEqual(0);
        expect(calculateBonuses("Invalid", 99999)).toEqual(0);
        done()
    });

    test('Standard >= 100 000', (done) => {
        const multiplier = 0.05;
        const bonus = 2.5;
        expect(calculateBonuses("Standard", 100000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Standard", 1000000)).toEqual(bonus * multiplier);
        done()
    });

    test('Premium >= 100 000', (done) => {
        const multiplier = 0.1;
        const bonus = 2.5;
        expect(calculateBonuses("Premium", 100000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Premium", 1000000)).toEqual(bonus * multiplier);
        done()
    });

    test('Diamond >= 100 000', (done) => {
        const multiplier = 0.2;
        const bonus = 2.5;
        expect(calculateBonuses("Diamond", 100000)).toEqual(bonus * multiplier);
        expect(calculateBonuses("Diamond", 1000000)).toEqual(bonus * multiplier);
        done()
    });

    test('Invalid >= 100 000', (done) => {
        const multiplier = 0;
        const bonus = 2.5;
        expect(calculateBonuses("Invalid", 100000)).toEqual(0);
        expect(calculateBonuses("Invalid", 1000000)).toEqual(0);
        done()
    })
});